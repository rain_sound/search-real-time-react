import React, { Component } from "react";

const songs = [
  {
    id: 1,
    nombre: "LAST SONG"
  },
  {
    id: 2,
    nombre: "NO VOLVERE"
  },
  {
    id: 3,
    nombre: "VIVIR BAILANDO"
  },
  {
    id: 4,
    nombre: "TENGO TODO DE TI"
  },
  {
    id: 5,
    nombre: "BESAME"
  },
  {
    id: 6,
    nombre: "BESAME2"
  }
];

export class ListSongs extends Component {
  constructor() {
    super();
    this.state = {
      songs: [],
      filteredSongs: songs,
      search: ""
    };
  }

  componentDidMount() {
    this.setState({ songs });
  }

  handleChange = e => {
    const { value } = e.target;
    this.setState({ search: value });
    this.songsWillFiltered();
    if (value.length === 0) {
      this.setState({ filteredSongs: songs });
    }
    else this.songsWillFiltered()
  };

  songsWillFiltered = () => {
    const { songs, search } = this.state;
    const filteredSongs = songs.filter(song => song.nombre.includes(search.toUpperCase()));
    this.setState({ filteredSongs });
  };
  render() {
    const { filteredSongs } = this.state;
    return (
      <div className="row justify-content-center">
        <div className="col-4">
          <input
            className="form-group"
            type="text"
            placeholder="Ingrese una cancion"
            onChange={this.handleChange}
          />
          {filteredSongs.map(song => (
            <li key={song.id}>{song.nombre.toLowerCase()}</li>
          ))}
        </div>
      </div>
    );
  }
}

export default ListSongs;
