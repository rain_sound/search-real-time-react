import React, { Component } from "react";

import Info from "./components/Info";
import ListSongs from "./components/ListSongs";

class App extends Component {
  render() {
    return (
      <div className="container">
        <Info />
        <ListSongs />
      </div>
    );
  }
}

export default App;
